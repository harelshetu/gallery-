const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const app = express();
const sequelize = require('./util/dataBase');
const picturesTable = require('./models/picture');

// for handlingreq.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


// adding routes
const homePageRoutes =require('./routes/homePage');
const searchPageRoutes = require('./routes/searchPage');
const addPageRoutes = require('./routes/addPage');
const editPageRoutes = require('./routes/editPage');


//define routes
app.use('/api',homePageRoutes);
app.use( '/api',searchPageRoutes);
app.use('/api',addPageRoutes);
app.use('/api',editPageRoutes);

//TODO didn't know if necessary to do app.use(errorController.get404);


sequelize.sync() // create table if don't exist
    .then(result => {
      console.log("sucessssss");
      app.listen(5000);
      sequelize
          .authenticate()
          .then(() => {
            console.log('Connection has been established successfully.');
          })
          .catch(err => {
            console.error('Unable to connect to the database:', err);
          });
      picturesTable.create({
        title: 'first',
        pic_url: 'http://i65.tinypic.com/rj03zq.jpg'
      }).then(result => {
        console.log('Saved the first image  ');
      })
          .catch(err => {
            console.log(err);
          });

    }).catch(err => {
  console.log(err.sql+ "         sequelize failed");
});

