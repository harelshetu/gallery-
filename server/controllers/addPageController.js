
const picturesTable = require('../models/picture');

exports.getAddPage = (req,res,next)=>
{
    //console.log("in add page controller");
    // add image only if the title not in used
    picturesTable.find({where:{title:req.body.pictureTitle}})
        .then(res=>{
            if(!res)
            {
                picturesTable.create({
                    title: req.body.pictureTitle,
                    pic_url: req.body.pictureUrl
                }).then(() => {
                    console.log('picture created');
                })
            }
            else{
                console.log('not created');

            }
        })

};