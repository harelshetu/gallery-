
const picturesTable = require('../models/picture');

exports.getHomePage = (req,res,next)=>
{

   // console.log("in home page controller");

    // search for all images in db
    picturesTable.findAll()
        .then(allpics => res.json(allpics))
        .catch(err=> console.log(err+"didn't find"))

};

exports.removeImage = (req,res,next)=>
{

    console.log("in  removeImage");
    console.log(req.body);
    console.log(req.body.imageId);

    //delete index
    picturesTable
      .destroy({
        where: {
          id: req.body.imageId
        },
        truncate: false
      })
      .then(() => console.log("removed"))

      .catch(err => console.log(err+" remove image failed"));

};