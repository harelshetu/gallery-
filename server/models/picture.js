const Sequelize = require('sequelize');

const sequelize = require('../util/database');

// create the table
const pictureTable = sequelize.define(
    'pics', {
        id:{
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        title:{
            type: Sequelize.STRING,
            allowNull: false

        },
        pic_url: {
            type: Sequelize.STRING,
            allowNull: false
        }}
        );
//    });
module.exports = pictureTable;

/*
,
        createdAt: {
            field: 'created_at',
            type: Sequelize.DATE,
        },
        updatedAt: {
            field: 'updated_at',
            type: Sequelize.DATE,
        }
 */
