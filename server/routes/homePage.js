const express = require("express");
const homePageController = require('../controllers/homePageController');
const router = express.Router();


console.log("in home page route");

router.get('/',homePageController.getHomePage);
router.post('/',homePageController.removeImage);

module.exports = router;

