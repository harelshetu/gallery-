import React, { Component } from 'react';
import Nav from './Nav'
import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom'
import Home from './components/HomeComponent';
import Search from './components/SearchComponent';
import Add from './components/AddComponent';
import NotFound from './components/NotFoundComponent';
import Edit from './components/EditComponent'

class App extends Component {

    state = {

        images:[]
    };


    render= () => {
        console.log(this.state.images);
        return (
          <Router>
            <div className="App">
              <Nav />
              <div>
                  <Switch>
                <Route path="/" exact={true} render={()=>
                    (<Home {...this.state} data = {this.state.images} />)}/>
                <Route path="/search" component={Search} />
                <Route path="/add" component={Add} />
                <Route path="/edit" component={Edit}/>
                <Route component={NotFound}/>
                  </Switch>
              </div>
            </div>
          </Router>
        );
    }
}

export default App;


