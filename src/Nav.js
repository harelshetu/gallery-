import React from 'react';
import { Link } from 'react-router-dom';


export default class Nav extends React.Component {
    render() {
        return (

            <div className="container">
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <Link  className="navbar-brand" to="/">Home</Link>
                    <Link className="navbar-brand" to="/search">Search</Link>
                    <Link className="navbar-brand" to="/add">Add</Link>
                </nav>
            </div>
    );
    }
}

