import React from 'react';
import { withRouter } from "react-router-dom";


class Add extends React.Component {

    state = {
        pictureTitle :'',
        pictureUrl: '',
        pictureTitleError:'',
        pictureUrlError:''
    };


    handleInputChange = (event) =>{
        console.log("in handle input");
        //console.log(event.target.value);

        this.setState({
            [event.target.name]:event.target.value.toLowerCase() // store the search field value in searchValue
        });
     //   console.log(this.state);


    };


    validate =()=>
    {
        let pictureTitleError = '',
            pictureUrlError = '';

        if(this.state.pictureTitle.length > 15)
            pictureTitleError='Too long string, maximum charecters:15';

       if(this.state.pictureUrl.indexOf(".") === -1)
           pictureUrlError = 'please enter an image';
        else{
           let extension = this.state.pictureUrl.substring(
               this.state.pictureUrl.lastIndexOf('.')+1)
               .toLowerCase();

           if(!(extension  === "jpeg" || extension  === "png" ||extension  === "gif" ||
               extension  === "jpg") )
               pictureUrlError = ' only the next format are allowed: jpeg, png, gif format';

       }


        if(pictureTitleError || pictureUrlError)
        {
            this.setState({pictureTitleError, pictureUrlError});
            return false;
        }

        return true;
    };



    handleSubmit = (event)=>{
        console.log("in handle submit");
        event.preventDefault();
        const isValid= this.validate();

        if(isValid) {

            fetch('/api/add', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },

                body: JSON.stringify(this.state)
            })
                .then(res =>
                    res.json()
                )
                .then(res => {
                    console.log(res);
                })
                .catch(err => console.log(err + "  send form failed"));
            window.location.reload();
            this.props.history.push("/"); // redirect to home page
        }


        this.setState({
            pictureTitle:"",
            pictureUrl:""
        })


    };



    render = () => {

        return (

            <form style={{
                width: "300px",
                fontSize: '15px', position: "absolute",
                left: "400px"
            }}
            onSubmit={(e)=>{this.handleSubmit(e)} }
            >
                <div className="form-group">
                    <label htmlFor="formGroupExampleInput">Image title:</label>
                    <input type="text" className="form-control"
                           id="formGroupExampleInput" placeholder="Enter image title"
                           name="pictureTitle" value={this.state.pictureTitle}
                    required
                           onChange={this.handleInputChange}/>
                           <div style={{color:'red'}}>{this.state.pictureTitleError}</div>

                    <label htmlFor="formGroupExampleInput2">Image url:</label>
                    <input type="text" className="form-control"
                           id="formGroupExampleInput2" placeholder="Enter image url"
                           name="pictureUrl" value={this.state.pictureUrl}
                    required
                           onChange={this.handleInputChange}/>
                    <div style={{color:'red'}}>{this.state.pictureUrlError}</div>
                </div>
                <button type="submit">Add image</button>
            </form>


        );
    }
}
export default withRouter(Add);