import React from 'react';
import { withRouter } from "react-router-dom";

class Edit extends React.Component{

    state={
        pictureTitle :'',
        picToChangeId:this.props.location.box,
        pictureTitleError:""
    };

    validate = () => {
      //  console.log( "IN VALIDATE");
        let pictureTitleError = "";

        if (this.state.pictureTitle.length > 15)
            pictureTitleError = "Too long string, maximum characters:15";

        if (pictureTitleError) {
            this.setState({pictureTitleError});
            console.log("err..."+this.state.pictureTitleError);
            return false;
        }
        return true;
    };

    handleInputChange =(event)=>
    {
        this.setState({
            [event.target.name]:event.target.value.toLowerCase() // store the search field value in searchValue
        });
    };


    handleSubmit = (event)=>
    {
        event.preventDefault();
        console.log(  this.state);

        const isValid = this.validate();

        if(isValid) {
            fetch('/api/edit', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(this.state)
            })
                .then(res =>
                    res.json()
                )
                .then(res => {
                    console.log(res);
                })
                .catch(err => console.log(err + "  send form failed"));
            this.props.history.push("/"); // redirect to home page
        }
        //console.log("2.."+this.state.pictureTitleError);

    };


    render() {
        return(

            <form className="form-inline"
                  onSubmit={(e)=>this.handleSubmit(e)}>
                <div className="form-group mb-2"  >
                    <input type="text" className="form-control mb-2 mr-sm-2" id="inlineFormInputName2"
                           placeholder="Enter image title"
                           name="pictureTitle" value={this.state.pictureTitle}
                           required
                           onChange={this.handleInputChange}
                           style={{
                               width: "200px",
                               fontSize: '15px',
                               marginLeft: "300px",
                           }}
                    />

                </div>
                <button type="submit" className="btn btn-primary mb-2"
                        style={{
                            width: "100px",
                            fontSize: '15px',
                        }}>Edit title</button>
                <div style={{color:'red',
                    marginLeft: "310px",
                    fontSize: '15px'}}>{this.state.pictureTitleError}</div>
            </form>
        );
    }


}

export default withRouter(Edit);

/*<form style={{

        }}
              onSubmit={(e)=>this.handleSubmit(e)  }>
            <div className="form-group">
                <label htmlFor="formGroupExampleInput">Image title:</label>
                <input type="text" className="form-control"
                       id="formGroupExampleInput" placeholder="Enter new image title"
                       name="pictureTitle" value={this.state.pictureTitle}
                       required
                       onChange={this.handleInputChange}/>
                <div style={{color:'red'}}>{this.state.pictureTitleError}</div>
            </div>
            <button type="submit"
                    style={{
                        width: "200px",
                        fontSize: '15px',
                        marginLeft: "300px",
                        marginTop:'0px'
                    }}>Edit image title</button>
        </form>
*/
//{this.state.pictureTitleError}