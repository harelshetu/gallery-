import React from 'react';
import {Link} from "react-router-dom";
import './HomeComponent.css'


class Home extends React.Component {
  state = {
    images: []
  };

  componentDidMount() {
    fetch("/api")
      .then(res => res.json())
      .then(images => this.setState({ images }))
      .catch(err => console.log(err + "__________hereee"));
  }

  handleClick = obj => {
    console.log(obj);

    fetch("/api", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },

      body: JSON.stringify(obj)
    })
      .then(res => res.json())
      .catch(err => console.log(err + " remove failed"));

    window.location.reload(); // refresh page
  };

  renderImages = () => {
    if (this.state.images.length === 0)
        return <p>please add picture</p>;

    return this.state.images.map(pic => (
      <div className="col-md-6 col-lg-4">
        <div className="card border-0 transform-on-hover" key={pic.id}>
          <img src={pic.pic_url} alt="not loaded" className="card-img-top" />
          <div className="card-body">
            <h6>{pic.title}</h6>
            <button
              className="btn btn-primary m-1"
              type="submit"
              style={{ width: "100%" }}
              onClick={() => this.handleClick({ imageId: Number(pic.id) })}
            >
              Remove
            </button>
            <Link to={{ pathname: "/edit", box: pic.id }}>
              <button
                className="btn btn-primary m-1"
                type="submit"
                style={{ width: "100%" }}
              >
                Edit
              </button>
            </Link>
          </div>
        </div>
      </div>
    ));
  };

  render = () => {
    return (
      <section className="gallery-block cards-gallery">
        <div className="container">
            <div className="heading">
                <h1>Gallery</h1>
            </div>
          <div className="row">{this.renderImages()}</div>
        </div>
      </section>
    );
  };
}
export default Home;

