import React from 'react';
import {Link} from "react-router-dom";


class Search extends React.Component {
  state = {
      searchValue: "", // the field in the search form
      searchResult: [], // the result of search
      first: true, // in order to show search result only when the user hit submit
      searchValueError: "",
      notFoundMassage:"Image not found"
  };

  validate = () => {
    let searchValueError = "";

    if (this.state.searchValue.length > 15)
      searchValueError = "Too long string, maximum charecters:15";

    if (searchValueError) {
      this.setState({ searchValueError });
      return false;
    }

    return true;
  };

  handleInputChange = event => {
    console.log("in handle input");
    this.setState({
      [event.target.name]: event.target.value.toLowerCase() // store the user input in state
    });
    console.log("searchvalue is  :" + this.state);
  };

  componentDidMount() {
    // console.log("searchvalue is ftech :"+(this.state.searchValue));

    fetch("/api/search", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },

      body: JSON.stringify(this.state)
    })
      .then(res => res.json())
      .then(res => {
        console.log(res);
        if (res.length !== 0) {
          this.setState({ searchResult: res,
              notFoundMassage:"",
              searchValueError:""
                });
          //console.log(this.state.searchResult);
        } else
          this.setState({
            searchResult: [],
              notFoundMassage:"Image not found",
              searchValueError:""
          });

      })
      .catch(err => console.log(err + "  send form failed"));
  }

  handleSubmit = event => {
    event.preventDefault(); // prevent rendering the page and showing the form data in the url which is the default

    const isValid = this.validate();
    if (isValid) {
      this.componentDidMount();
    }
    this.setState({
      searchValue: "",
        notFoundMassage:"",
        searchResult:"",
        first: false
    });
  };


  showAnswer = () => {
    if (!this.state.first) {
      if (this.state.searchResult.length === 0)
        return (
          <h2
            style={{
              width: "300px",
              fontSize: "15px",
              position: "absolute",
              left: "400px",
              top: "130px"
            }}
          >
              {this.state.notFoundMassage}
          </h2>
        );


        return this.state.searchResult.map(pic => (
            <section className="gallery-block cards-gallery">
                <div className="container">
                    <div className="row">
            <div className="col-md-6 col-lg-4">
                <div className="card border-0 transform-on-hover" key={pic.id}>
                    <img src={pic.pic_url} alt="not loaded" className="card-img-top" />
                    <div className="card-body">
                        <h6>{pic.title}</h6>
                    </div>
                </div>
            </div>
                    </div>
                </div>
            </section>
        ));

    }
  };

  render = () => {
    //console.log("in search");
    return (
      <div>
        <form
          className="form-inline"
          method="POST"
          style={{
            width: "300px",
            fontSize: "13px",
            position: "absolute",
            left: "400px"
          }}
          onSubmit={e => this.handleSubmit(e)}
        >
          <div className="form-group">
            <input
              type="text"
              className="form-control"
              placeholder={"enter image title"}
              name="searchValue"
              value={this.state.searchValue}
              id={"searchBox"}
              style={{ padding: "13px" }}
              required
              onChange={this.handleInputChange}
            />
          </div>
          <button type="submit" value="submit">
            search image
          </button>
        </form>
        <div
          style={{
            width: "300px",
            fontSize: "15px",
            position: "absolute",
            left: "400px",
            top: "130px",
            color: "red"
          }}
        >
          {this.state.searchValueError}
        </div>
        <div>
          {this.showAnswer()}
        </div>
      </div>
    );
  };
}
export default Search;


