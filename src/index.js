import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { BrowserRouter } from 'react-router-dom'
import Bootstrap from 'bootstrap/dist/css/bootstrap.css'

const router = (
    <BrowserRouter>
        <App />
    </BrowserRouter>
)

ReactDOM.render(router, document.getElementById('root'));
